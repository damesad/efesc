<?php

/**
 * Strings for component 'theme_efesc', language 'en'
 *
 * @package   theme_efesc
 * @copyright 2016 Diego Mesa
 * @license   http://creativecommons.org/licenses/by-nc/4.0/legalcode Creative Commons Attribution-NonCommercial 4.0 Internationa
 */

$string['choosereadme'] = '
<div class="clearfix">
<div class="well">
<h2>Efesc</h2>
<p><img class=img-polaroid src="efesc/pix/screenshot.jpg" /></p>
</div>
<div class="well">
<h3>Sobre el tema : </h3>
<<<<<<< HEAD
<p>Efesc usa como base el tema boostrapbase que fue un tema creado en moodle 2.5 para diseño responsive.</p>
<p>Efesc esta orientado para mantener la atención del usuario en el contenido del curso  y evitar distracciones, 
ademas esta optimizado para el uso en dispositivos móviles</p>
<p>Este tema esta licenciado sobre la licencia **Creative Commons Attribution-NonCommercial 4.0 International** 
lo que quiere decir que:</p>
<ol>
<li>
<p>Debe reconocer y citar en la obra todos los autores de la obra</p>
<pre>
ejemplo : 
	@copyright  2013 Moodle, moodle.org ...
	@copyright 2016 Diego Mesa ...
</pre>
</li>
<li>
<p>Pueder realizar copias, distribuciones y modificacione sin fines comerciales.
<strong>No puede vender el tema, si puede cobrar por instalacion o mantenimiento</strong></p>
</li>
</ol>
<h3>Creditos del Tema</h3>
<p>Autores : Diego Mesa - @damesad <br>
</p>
</div>
</div>';

$string['configtitle'] = 'efesc';

$string['customcss'] = 'Custom CSS';
$string['customcssdesc'] = 'Whatever CSS rules you add to this textarea will be reflected in every page, making for easier customization of this theme.';

$string['footnote'] = 'Footnote';
$string['footnotedesc'] = 'Whatever you add to this textarea will be displayed in the footer throughout your Moodle site.';

$string['invert'] = 'Invert navbar';
$string['invertdesc'] = 'Swaps text and background for the navbar at the top of the page between black and white.';

$string['logo'] = 'Logo';
$string['logodesc'] = 'The logo is only displayed in the header of the front page and login page.<br /> If the height of your logo is more than 75px add div.logo {height: 100px;} to the Custom CSS box below, amending accordingly if the height is other than 100px.';

$string['pluginname'] = 'efesc';

$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';

