<?php
/**
 * Moodle's efesc theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package   theme_clean
 * @copyright 2013 Moodle, moodle.org
 * @license   http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * @package   theme_efesc
 * @copyright 2016 Diego Mesa
 * @license   http://creativecommons.org/licenses/by-nc/4.0/legalcode Creative Commons Attribution-NonCommercial 4.0 Internationa
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {

    // Invert Navbar to dark background.
    $name = 'theme_efesc/invert';
    $title = get_string('invert', 'theme_efesc');
    $description = get_string('invertdesc', 'theme_efesc');
    $setting = new admin_setting_configcheckbox($name, $title, $description, 0);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Logo file setting.
    $name = 'theme_efesc/logo';
    $title = get_string('logo','theme_efesc');
    $description = get_string('logodesc', 'theme_efesc');
    $setting = new admin_setting_configstoredfile($name, $title, $description, 'logo');
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Custom CSS file.
    $name = 'theme_efesc/customcss';
    $title = get_string('customcss', 'theme_efesc');
    $description = get_string('customcssdesc', 'theme_efesc');
    $default = '';
    $setting = new admin_setting_configtextarea($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);

    // Footnote setting.
    $name = 'theme_efesc/footnote';
    $title = get_string('footnote', 'theme_efesc');
    $description = get_string('footnotedesc', 'theme_efesc');
    $default = '';
    $setting = new admin_setting_confightmleditor($name, $title, $description, $default);
    $setting->set_updatedcallback('theme_reset_all_caches');
    $settings->add($setting);
}
