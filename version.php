<?php
/**
 * Moodle's Clean theme, an example of how to make a Bootstrap theme
 *
 * DO NOT MODIFY THIS THEME!
 * COPY IT FIRST, THEN RENAME THE COPY AND MODIFY IT INSTEAD.
 *
 * For full information about creating Moodle themes, see:
 * http://docs.moodle.org/dev/Themes_2.0
 *
 * @package    theme_clean
 * @copyright  2013 Moodle, moodle.org
 * @license    http://www.gnu.org/copyleft/gpl.html GNU GPL v3 or later
 *
 * @package   theme_efesc
 * @copyright 2016 Diego Mesa
 * @license   http://creativecommons.org/licenses/by-nc/4.0/legalcode Creative Commons Attribution-NonCommercial 4.0 Internationa
 */

defined('MOODLE_INTERNAL') || die;

$plugin->version   = 2016010800;
$plugin->requires  = 2015111000;
$plugin->component = 'theme_efesc';
$plugin->dependencies = array(
    'theme_bootstrapbase'  => 2015111000,
);
