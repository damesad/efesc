**Tema Efesc**
======

*Sobre el tema :*
-----
Efesc usa como base el tema boostrapbase que fue un tema creado en moodle 2.5 para diseño responsive. 

Efesc esta orientado para mantener la atención del usuario en el contenido del curso  y evitar distracciones, ademas esta optimizado para el uso en dispositivos móviles

Este tema esta licenciado sobre la licencia **Creative Commons Attribution-NonCommercial 4.0 International** lo que quiere decir que:

1. Debe reconocer y citar en la obra todos los autores de la obra
> ejemplo : 
>@copyright  2013 Moodle, moodle.org ...
>@copyright 2016 Diego Mesa ...

2. Puede realizar copias, distribuciones y modificaciones sin fines comerciales.
*No puede vender el tema, si puede cobrar por instalación o mantenimiento*

*Autores :*
-----
 Diego Mesa - @damesad