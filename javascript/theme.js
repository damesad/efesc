/**
 * =========================
 * copyright 2016 Diego Mesa
 * license  Creative Commons Attribution-NonCommercial 4.0 Internationa
 * http://creativecommons.org/licenses/by-nc/4.0/legalcode
 * =========================
*/
require(['jquery'], function($) 
{
	$menu = $("#block-region-side-pre")
	$page = $("#page")
	$page.addClass("side-pre-off")
	$menu.addClass("side-pre-off")
	$menu.prepend('<div id="toogle-side-pre"></div>')
		
	function tooglesidepre() {
		var estado = "side-pre-off"

			$menu.toggleClass(estado);
			if($menu.hasClass(estado))
			{
				$page.addClass(estado)
			}
			else
			{
				$page.removeClass(estado)
			}
	}

	$('#toogle-side-pre').click(function(){
		tooglesidepre();
	});
		
	$('.block .header .title').click(function(){
		tooglesidepre();
	});

})